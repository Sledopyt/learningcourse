nk = list(map(int, input().split()))
n = list(map(int, input().split()))
k = list(map(int, input().split()))

for elements_k in k:
    left = 0
    right = len(n) - 1
    while right - left > 1:
        med = (left + right) // 2
        if n[med] > elements_k:
            right = med
        elif n[med] < elements_k:
            left = med
        else:
            result = n[med]
            break
    if right - left == 1:
        if elements_k - n[left] <= n[right] - elements_k:
            result = n[left]
        else:
            result = n[right]
    print(result)

