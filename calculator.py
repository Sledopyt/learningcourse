def sl(a, b):
    itog = a + b
    return itog

def vych(a, b):
    itog = a - b
    return itog

def um(a, b):
    itog = a * b
    return itog

def delen(a, b):
    itog = a / b
    return itog

def st(a, b):
    itog = a ** b
    return itog

def delim():
    print("=" * 60)

dct = {1 : "сложения",
       2 : "вычитания",
       3 : "умножения",
       4 : "деления",
       5 : "возведения в степень"}

while True:
    print("Укажите требуемое действие (1-6)")
    print("1 : Сложение")
    print("2 : Вычитание")
    print("3 : Умножение")
    print("4 : Деление")
    print("5 : Возведение в степень")
    print("6 : Выход")
    delim()
    MenuItem = int(input())
    if MenuItem == 6 : break
    if MenuItem > 0 and MenuItem < 6 :
        try:
            #print("Введите первое число:")
            #f = int(input())
            f = float(input("Введите первое число : "))
            #print("Введите второе число:")
            #s = int(input())
            s = float(input("Введите второе число : "))
            if MenuItem == 1:
                result = sl(f, s)
            elif MenuItem == 2:
                result = vych(f, s)
            elif MenuItem == 3:
                result = um(f, s)
            elif MenuItem == 4:
                result = delen(f, s)
            else:
                result = st(f, s)
            print("Результат", dct[MenuItem], ":", result)
            delim()
        except OverflowError:
            delim()
            print("Результат", dct[MenuItem], "не может быть получен - переполнение")
            delim()
        except ZeroDivisionError:
            delim()
            print("Результат", dct[MenuItem], "не может быть получен - деление на 0")
            delim()
        except ValueError:
            delim()
            print("Результат", dct[MenuItem], "не может быть получен - введено не число")
            delim()
    else:
        print("Указано несуществующее действие!")
        delim()
