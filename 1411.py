k = int(input())
n_lst = list(map(int, input().split()))
n = len(n_lst)
iteration = 0

for i in range(n-1):
    for j in range(n-1-i):
        if n_lst[j] > n_lst[j+1]:
            n_lst[j], n_lst[j+1] = n_lst[j+1], n_lst[j]
            iteration += 1

print(iteration)

