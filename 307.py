def min_of_4(a, b, c, d):
    itog = a
    if b <= itog:
        itog = b
    if c <= itog:
        itog = c
    if d <= itog:
        itog = d
    return itog

s = input()
lst = []
lst.extend(s.split(" "))

print(min_of_4(int(lst[0]), int(lst[1]), int(lst[2]), int(lst[3])))
