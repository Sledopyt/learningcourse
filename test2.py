import datetime
import sqlalchemy as sa
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker


Base = declarative_base()


class Client(Base):
    __tablename__ = "clients"  # Клиенты

    id = sa.Column(sa.Integer, primary_key=True)
    name = sa.Column(sa.String(255), nullable=False)
    address = sa.Column(sa.String(255))
    phone = sa.Column(sa.String(255))


class Worker(Base):
    __tablename__ = "workers"  # Список сотрудников

    id = sa.Column(sa.Integer, primary_key=True)
    family = sa.Column(sa.String(64), nullable=False)
    name = sa.Column(sa.String(64), nullable=False)
    sname = sa.Column(sa.String(64), nullable=False)
    dept_id = sa.Column(sa.Integer, nullable=False)


class Result(Base):
    __tablename__ = "results"  # Справочник результатов работы по тикетам

    id = sa.Column(sa.Integer, primary_key=True)
    result = sa.Column(sa.String(64), nullable=False)


class Call(Base):
    __tablename__ = "calls"  # Тикеты

    id = sa.Column(sa.Integer, primary_key=True)
    operator_id = sa.Column(
        sa.Integer,
        sa.ForeignKey(f"{Worker.__tablename__}.id"), nullable=False
    )
    client_id = sa.Column(sa.Integer, sa.ForeignKey(f"{Client.__tablename__}.id"))
    call_date = sa.Column(sa.Date, nullable=False)
    problem = sa.Column(sa.String(255), nullable=False)
    worker_id = sa.Column(sa.Integer, sa.ForeignKey(f"{Worker.__tablename__}.id"), nullable=True)
    work_date = sa.Column(sa.Date, nullable=True)
    result_id = sa.Column(sa.Integer, sa.ForeignKey(f"{Result.__tablename__}.id"), nullable=True)


class Role:
    Session = sessionmaker()

    def __init__(self, url):
        self.engine = sa.create_engine(url)
        self.session = self.Session(bind=self.engine)
        Base.metadata.create_all(self.engine)


class OperatorRole(Role):
    def incoming_call(self, operator_id, client_id, problem):
        self.session.add(Call(
            operator_id=operator_id,
            client_id=client_id,
            call_date=datetime.datetime.now(),
            problem=problem,
            ))
        self.session.commit()


class AdminRole(Role):
    def call_distribution(self, call_id, worker_id):
        self.session.query(Call).filter(Call.id == call_id).update({Call.worker_id: worker_id})
        self.session.commit()

    def add_workers(self, family, name, sname, dept_id):
        self.session.add(
            Worker(family=family, name=name, sname=sname, dept_id=dept_id),
        )
        self.session.commit()

    def add_clients(self, name, address=None, phone=None):
        self.session.add(
            Client(name=name, address=address, phone=phone),
        )
        self.session.commit()

    def add_results(self, result):
        self.session.add(
            Result(result=result)
        )
        self.session.commit()


class WorkerRole(Role):
    def result_update(self, call_id, result_id):
        self.session.query(Call).filter(Call.id == call_id).update({
            Call.result_id: result_id,
            Call.work_date: datetime.datetime.now()
            })
        self.session.commit()


admin = AdminRole(url="sqlite:///db_test.sqlite3")
operator = OperatorRole(url="sqlite:///db_test.sqlite3")
worker = WorkerRole(url="sqlite:///db_test.sqlite3")


admin.add_workers("Иванов", "Иван", "Иванович", 1)
admin.add_workers("Петров", "Петр", "Петрович", 1)
admin.add_workers("Сидоров", "Сидор", "Сидорович", 1)
admin.add_workers("Лютикова", "Роза", "Фиалковна", 2)
admin.add_workers("Булкина", "Пышка", "Пончиковна", 2)
admin.add_clients(name="ООО ДМС", address="Невский 46", phone="123-45-67")
admin.add_clients(name="ИП Чебурашкин", phone="+7 (911) 987-6543")
admin.add_results("Проблема решена")
admin.add_results("Проблема не решена")


operator.incoming_call(4, 1, "Нет сигнала на линии")
operator.incoming_call(5, 2, "Всё пропало, ничего толком объяснить не могут")

admin.call_distribution(1, 3)
admin.call_distribution(2, 2)

worker.result_update(1, 2)
worker.result_update(2, 1)

