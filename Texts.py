# collections

a = input()
b = input()
c = input()

st1 = set(a)
st2 = set(b)
st3 = set(c)

print(len(st1))
print(len(st2))
print(len(st3))
print(len(st1 & st2 & st3))
print(len(st1 | st2 | st3))
