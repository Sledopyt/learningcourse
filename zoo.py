class Cage:
    def __init__(self, space):
        self.space = space
        self.animal_list = []

    def add(self, animal) -> bool:
        if animal.space > self.space:
            return False
        self.space = self.space - animal.space
        self.animal_list.append(animal.name)
        return True

    def free_space(self) -> int:
        return self.space

    def get_animals(self) -> list:
        return self.animal_list


class Animal:
    def __init__(self, name, space):
        self.name = name
        self.space = space


cage = Cage(200)
lion = Animal("Alex", 100)
pinguin1 = Animal("Gunter", 15)
pinguin2 = Animal("Ganter", 25)
begemoth = Animal("Gloria", 300)
giraffe = Animal("Melvin", 70)

cage.add(pinguin1)      # True
cage.add(begemoth)      # False
cage.free_space()       # 185
cage.add(lion)          # True
cage.free_space()       # 85
cage.add(pinguin2)      # True
cage.add(giraffe)       # False
cage.get_animals()      # ['Gunter', 'Alex', 'Ganter']
print(cage.get_animals())
