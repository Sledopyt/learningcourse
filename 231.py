nk = list(map(int, input().split()))
n = list(map(int, input().split()))
k = list(map(int, input().split()))

n_new = n[:(k[1]-1)] + k[:1] + n[(k[1]-1):]
result = ''

for elements in n_new:
    result += ' ' + str(elements)

print(result.lstrip())
