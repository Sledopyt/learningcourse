import math
import requests
import sys

from PyQt5 import QtCore, QtGui, QtWidgets

class MainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        self.setGeometry(100,100,500,600)
        self.font = QtGui.QFont("Times New Roman", 20)

        self.citynamelabel = QtWidgets.QLabel(self)
        self.citynamelabel.setGeometry(50, 30, 75, 25)
        self.citynamelabel.setText("Мой город :")
        self.citynamelabel.adjustSize()

        self.citynameinput = QtWidgets.QLineEdit(self)
        self.citynameinput.setGeometry(150, 30, 200, 25)

        self.getinfobutton = QtWidgets.QPushButton(self)
        self.getinfobutton.setGeometry(380, 30, 75, 25)
        self.getinfobutton.setText("Узнать")

        self.citytemplabel = QtWidgets.QLabel(self)
        self.citytemplabel.setGeometry(50, 60, 75, 25)
        self.citytemplabel.setText("Температура :")
        self.citytemplabel.adjustSize()

        self.citytempinput = QtWidgets.QLineEdit(self)
        self.citytempinput.setGeometry(150, 60, 200, 25)

        self.windlabel = QtWidgets.QLabel(self)
        self.windlabel.setGeometry(35, 100, 30, 10)
        self.windlabel.setText("ВЕТЕР")

        self.windspeedlabel = QtWidgets.QLabel(self)
        self.windspeedlabel.setGeometry(35, 120, 100, 15)
        self.windspeedlabel.setText("скорость : ")

        self.winddirectionlabel = QtWidgets.QLabel(self)
        self.winddirectionlabel.setGeometry(35, 140, 100, 15)
        self.winddirectionlabel.setText("направление : ")
        
        self.windetlabel = QtWidgets.QLabel(self)
        self.windetlabel.setGeometry(35, 480, 50, 10)
        self.windetlabel.setText("масштаб")        
        self.etline = self.create_etline(35, 505)
        self.windmlabel = QtWidgets.QLabel(self)
        self.windmlabel.setGeometry(60, 500, 50, 10)
        self.windmlabel.setText("1 м/с")         

        self.nwrlabel = QtWidgets.QLabel(self)
        self.nwrlabel.setGeometry(248, 100, 10, 10)
        self.nwrlabel.setText("С")

        self.wwrlabel = QtWidgets.QLabel(self)
        self.wwrlabel.setGeometry(35, 315, 10, 10)
        self.wwrlabel.setText("З")        

        self.swrlabel = QtWidgets.QLabel(self)
        self.swrlabel.setGeometry(248, 525, 10, 10)
        self.swrlabel.setText("Ю")

        self.ewrlabel = QtWidgets.QLabel(self)
        self.ewrlabel.setGeometry(458, 315, 10, 10)
        self.ewrlabel.setText("В")
        
        self.pen = QtGui.QPen(QtGui.QColor(0, 0, 0))
        self.brush = QtGui.QBrush(QtGui.QColor(255, 255, 255, 255))

        self.windrose = self.create_windrose(250, 120)
        self.windline = self.create_windline(0, 250, 320, 0)

        self.getinfobutton.clicked.connect(self.get_weather_info)

    def create_windrose(self, x, y):
        windrose = QtGui.QPolygon(8)
        windrose[0] = QtCore.QPoint(x, y)
        windrose[1] = QtCore.QPoint(x+25, y+175)
        windrose[2] = QtCore.QPoint(x+200, y+200)
        windrose[3] = QtCore.QPoint(x+25, y+225)
        windrose[4] = QtCore.QPoint(x, y+400)
        windrose[5] = QtCore.QPoint(x-25, y+225)
        windrose[6] = QtCore.QPoint(x-200, y+200)
        windrose[7] = QtCore.QPoint(x-25, y+175)
        return windrose

    def create_windline(self, l, x, y, degr):
        windline = QtCore.QLine()
        xe = l*math.cos(math.radians(degr))
        ye = l*math.sin(math.radians(degr))
        windline.setLine(x, y, round(x+xe), round(y+ye))
        return windline

    def create_etline(self, x, y):
        etline = QtCore.QLine()
        etline.setLine(x, y, x+10, y)
        return etline

    def get_weather_info(self):
        city = self.citynameinput.text()
        appid = "109d071cd2a0d0a0f5c9378e9b07cc86"

        response = requests.get(
            "http://api.openweathermap.org/data/2.5/weather",
            params={
                "q" : city,
                "appid" : appid,
                "lang" : "ru"
            },
        )
        data = response.json()
        temp = data["main"]["temp"] - 273
        self.citytempinput.setText("%.0f C" % temp)
        wind_speed = data["wind"]["speed"]
        wind_deg = data["wind"]["deg"]
        if wind_speed <= 5:
            wind_text = "слабый"
        elif wind_speed <= 14:
            wind_text = "умеренный"
        elif wind_speed <= 24:
            wind_text = "сильный"
        elif wind_speed <= 32:
            wind_text = "очень сильный"
        else:
            wind_text = "ураганный"
        self.windline = self.create_windline(wind_speed*10, 250, 320, wind_deg-90)
        self.windspeedlabel.setText(f"{wind_text}, {wind_speed} м/с")
        self.winddirectionlabel.setText(f"направление : {wind_deg} ")
        self.update()

    def paintEvent(self, event):
        painter = QtGui.QPainter(self)
        painter.setPen(self.pen)
        painter.setBrush(self.brush)  
        painter.drawPolygon(self.windrose)
        painter.drawLine(self.windline)
        painter.drawLine(self.etline)



app = QtWidgets.QApplication(sys.argv)

main_window = MainWindow()
main_window.setWindowTitle("Узнай погоду")

main_window.show()

sys.exit(app.exec_())