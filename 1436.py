k = int(input())
n_lst = list(map(int, input().split()))
n = len(n_lst)

for i in range(1, n):
    iterations = 0
    while i > 0 and n_lst[i] < n_lst[i-1]:
        n_lst[i], n_lst[i-1] = n_lst[i-1], n_lst[i]
        i -= 1
        iterations += 1

    if iterations > 0:
        result = ''

        for elements in n_lst:
            result += ' ' + str(elements)
        
        print(result.lstrip())

