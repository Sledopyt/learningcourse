import datetime as dt


def write(ops, fa, sa, result, logfile):
    lf = open(logfile, "at")
    print(dt.datetime.strftime(dt.datetime.now(), "%d.%m.%Y %H:%M:%S"),
          ops, "Первое число:", fa, "Второе число:", sa,
          "Результат:", result, file=lf)
    lf.close()


def clear(logfile):
    lf = open(logfile, "wt")
    lf.close()

