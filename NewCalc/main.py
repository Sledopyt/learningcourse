import calc.history as histops
import calc.operations as calcops


def delim():
    print("=" * 60)


def logreadmenu():
    print("Укажите требуемое действие (1-3)")
    print("1 : Посмотреть историю работы")
    print("2 : Очистить историю работы")
    print("3 : Возврат в предыдущее меню")
    logmenuitem = int(input())
    return logmenuitem


dct = {1 : ["сложения", "Сложение"],
       2 : ["вычитания", "Вычитание"],
       3 : ["умножения", "Умножение"],
       4 : ["деления", "Деление"],
       5 : ["возведения в степень", "Возведение в степень"]}

logfile = "NewCalc/history.txt"

while True:
    print("Укажите требуемое действие (1-7)")
    print("1 : Сложение")
    print("2 : Вычитание")
    print("3 : Умножение")
    print("4 : Деление")
    print("5 : Возведение в степень")
    print("6 : История работы")
    print("7 : Выход")
    delim()
    menuitem = int(input())
    if menuitem == 7:
        break
    if 0 < menuitem < 6:
        try:
            fv = float(input("Введите первое число : "))
            sv = float(input("Введите второе число : "))
            if menuitem == 1:
                result = calcops.sl(fv, sv)
            elif menuitem == 2:
                result = calcops.vych(fv, sv)
            elif menuitem == 3:
                result = calcops.um(fv, sv)
            elif menuitem == 4:
                result = calcops.delen(fv, sv)
            else:
                result = calcops.st(fv, sv)
            print("Результат", dct[menuitem][0], ":", result)
            delim()
            histops.write(dct[menuitem][1], fv, sv, result, logfile)
        except OverflowError:
            delim()
            print("Результат", dct[menuitem][0], "не может быть получен - переполнение")
            delim()
        except ZeroDivisionError:
            delim()
            print("Результат", dct[menuitem][0], "не может быть получен - деление на 0")
            delim()
        except ValueError:
            delim()
            print("Результат", dct[menuitem][0], "не может быть получен - введено не число")
            delim()
    if menuitem == 6:
        while True:
            lm = logreadmenu()
            if lm == 1:
                delim()
                lf = open(logfile, "rt")
                for line in lf:
                    print(line)
                lf.close()
                delim()
            if lm == 2:
                histops.clear(logfile)
                print("Очистка истории выполнена")
                delim()
            if lm == 3:
                delim()
                break
    if menuitem > 7:
        print("Указано несуществующее действие!")
        delim()

