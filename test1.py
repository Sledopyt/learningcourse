from random import randint


def coininput(gamer):
    while True:
        coins = int(input(gamer + " берет монеты : "))
        if 0 < coins < 3:
            return coins
        print("Указано неправильное число монет.")
        print("Повторите ввод.")
        print("=" * 40)


countcoins = int(input("Укажите исходное количество монет : "))
while True:
    print("=" * 40)
    print("Монет на столе :", countcoins)
    countcoins -= coininput("Игрок")
    if countcoins <= 0:
        print("Игрок победил!")
        break
    if randint(0, 100)%2 == 0 and countcoins >= 2:
        compcoins = 2
    else:
        compcoins = 1
    print("Компьютер берет монеты :", compcoins)
    countcoins -= compcoins
    if countcoins <= 0:
        print("Компьютер победил!")
        break

